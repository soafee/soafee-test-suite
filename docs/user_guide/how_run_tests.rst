..
 # Copyright (c) 2023, Linaro Ltd
 #
 # SPDX-License-Identifier: MIT

#########################
Run SOAFEE Test Suite
#########################

- ``soafee-test-suite-setup`` install the requirements by
  ``soafee-test-suite-setup``. Must be run once before ``soafee-test-suite``.
- ``soafee-test-suite`` run the SOAFEE Test Suite.

.. code-block:: bash

    $ git clone https://gitlab.com/soafee/soafee-test-suite.git
    $ cd soafee-test-suite
    $ sudo ./soafee-test-suite-setup run # run it once
    $ sudo ./soafee-test-suite run -r

``soafee-test-suite run`` works around the bats_ tool. Any options pass to run
will also be passed to bats_. Hint: ``run soafee-test-suite run -h`` to get
``bats`` help.

.. _bats: https://bats-core.readthedocs.io/en/stable/


===============================
Run SOAFEE Test Suite Test Plan
===============================

There are two defined test plans: ``soafee`` and ``virtualization``.

The ``soafee`` test plan follows the SOAFEE Architecture v1.0, the
``virtualization`` test plan follows the next architecture version.

To run one of the SOAFEE test plan, use the ``--filter-tags`` flag option:

.. code-block:: bash

    $ soafee-test-suite run --filter-tags soafee


=========================
Install SOAFEE Test Suite
=========================

``soafee-test-suite`` is self-contained with only awk as dependency. It will work without installation, but it can be installed.

.. code-block:: bash

    $ make install


===============================
SOAFEE Test Suite running in CI
===============================

`Every day SOAFEE Test Suite`_ is run in several platforms:

- AVA baremetal
- AVA Xen
- Qemu
- RockPi4
- Synquacer

.. _`Every day SOAFEE Test Suite`: https://qa-reports.linaro.org/blueprints/nightly/tests/soafee-test-suite/soafee-test-suite
