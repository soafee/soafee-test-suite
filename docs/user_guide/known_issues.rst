..
 # Copyright (c) 2023, Linaro Ltd
 #
 # SPDX-License-Identifier: MIT

#############
Known Issues
#############

K3s heavy CPU use
*****************

*TL;DR*: The high CPU we have experienced is expected because running Kubernetes
takes a lot of resources. If container orchestration is required directly in
HW, use Docker Compose or Podman Quadlet. Currently, K8s and K3s require more
resources than it's expected to be available in an embedded HW.

Brad Davidson's comments_, K3s Principal Maintainer, regarding the heavy CPU:

    Running Kubernetes is not free. It is expected that it will take about 0.5
    to 0.75 of a single core on an embedded system like a Raspberry Pi just as
    a baseline.

.. _comments: https://rancher-users.slack.com/archives/CGGQEHPPW/p1683031747740199

`There is also`_ regarding how much K3s requires of the CPU. The numbers there
match what Brad says.

.. _`There is also`: https://docs.k3s.io/reference/resource-profiling

It's important to be aware of this SW limitation because orchestrating
containers is expensive and badly done could quickly drain the car battery
charge.
