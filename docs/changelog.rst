..
 # Copyright (c) 2023, Linaro Limited.
 #
 # SPDX-License-Identifier: MIT

#########################
Changelog & Release Notes
#########################

v0.1 - 2023-06-21
~~~~~~~~~~~~~~~~~~~~~~
.. note::

  The first SOAFEE Test Suite release.

* Introduce soafee-test-suite and soafee-test-suite-setup as standalone tools
* Added initial Container tests
* Added initial Virtualization tests
* Tests running in TRS CI with supported platforms: RockPi4, Qemu, AVA
  baremetal, AVA + Xen Dom0 + Xen DomU, Synquacer
* Added SOAFEE Test Suite documentation
