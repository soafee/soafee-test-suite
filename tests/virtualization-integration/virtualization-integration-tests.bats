#!/usr/bin/env bats
#
# Copyright (c) 2021-2022, Arm Limited.
# Copyright (c) 2023, Linaro Limited.
#
# SPDX-License-Identifier: MIT

# Run-time validation tests for the Xen hypervisor implementing virtualization
# on a TRS system

# Set generic configuration

export TEST_LOG_FILE="${SOAFEE_LOG_DIR}/virtualization-integration-tests.log"
export TEST_STDERR_FILE="${SOAFEE_LOG_DIR}/virt-stderr.log"
export TEST_RUN_FILE="${SOAFEE_TMP_DIR}/virtualization-integration-tests.pgid"

# Set test-suite specific configuration

export TEST_GUEST_VM="guest-vm"
export TEST_GUEST_VM_CFG="${BATS_TEST_DIRNAME}"/create_xen_vm.cfg
export TEST_CLEAN_ENV="${VIRT_TEST_CLEAN_ENV:=1}"

load "${SOAFEE_TOP_DIR}"/lib/integration-tests-funcs.sh
load "${SOAFEE_TOP_DIR}"/lib/virtualization-funcs.sh

# There are no base clean-up activities required
# Function is defined and called so that it can be conditionally overridden
clean_test_environment() {
  _run destroy_guest_vm "${TEST_GUEST_VM}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL"
    return 1
  fi
}

# Runs once before the first test
setup_file() {
  _run test_suite_setup clean_test_environment
  if [ "${status}" -ne 0 ]; then
    log "FAIL"
    return 1
  fi
}

# Runs after the final test
teardown_file() {
  _run test_suite_teardown clean_test_environment
  if [ "${status}" -ne 0 ]; then
    log "FAIL"
    return 1
  fi
}

# bats file_tags=virtual
@test 'Check xendomains is running' {
  if ! is_xen; then
    skip "Platform not supported"
  fi

  _run is_xendomains_running
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

@test 'Create Guest VM' {
  if ! is_xen; then
    skip "Platform not supported"
  fi

  _run create_guest_vm "${TEST_GUEST_VM_CFG}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

@test 'Check Guest VM has booted' {
  if ! is_xen; then
    skip "Platform not supported"
  fi

  _run is_guest_vm_initialized "${TEST_GUEST_VM_NAME}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

@test 'Validate Guest VM is running' {
  if ! is_xen; then
    skip "Platform not supported"
  fi

  _run is_guest_vm_running "${TEST_GUEST_VM_NAME}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

@test 'Validate Guest VM is not running after paused' {
  if ! is_xen; then
    skip "Platform not supported"
  fi

  _run sudo -n xl pause "${TEST_GUEST_VM_NAME}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  fi

  _run is_not_guest_vm_running "${TEST_GUEST_VM_NAME}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

@test 'Validate Guest VM is running after unpaused' {
  if ! is_xen; then
    skip "Platform not supported"
  fi

  _run sudo -n xl unpause "${TEST_GUEST_VM_NAME}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  fi

  _run is_guest_vm_running "${TEST_GUEST_VM_NAME}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}
