#!/usr/bin/env bats
#
# Copyright (c) 2021-2022, Arm Limited.
# Copyright (c) 2022, Linaro Limited.
#
# SPDX-License-Identifier: MIT

# Trigger error when expanding unset variables
set -o nounset  # set -u

# Run-time validation tests for the container engine running on a EWAOL system

# Set generic configuration

export TEST_LOG_FILE="${SOAFEE_LOG_DIR}/container-engine-integration-tests.log"
export TEST_STDERR_FILE="${SOAFEE_LOG_DIR}/ce-stderr.log"
export TEST_RUN_FILE="${SOAFEE_TMP_DIR}/container-engine-integration-tests.pgid"
export TEST_IMAGE_ID="${SOAFEE_LOG_DIR}/container-engine-integration-tests-image.id"
export TEST_CONTAINER_ID="${SOAFEE_LOG_DIR}/container-engine-integration-tests.id"

# Set test-suite specific configuration

CE_TEST_IMAGE="registry.gitlab.com/soafee/soafee-test-suite/soafee-alpine"

export TEST_CLEAN_ENV="${CE_TEST_CLEAN_ENV:=1}"

load "${SOAFEE_TOP_DIR}"/lib/integration-tests-funcs.sh
load "${SOAFEE_TOP_DIR}"/lib/container-engine-funcs.sh

# Global container id used by the tests

# Ensure that the state of the container environment is ready for the test suite
clean_test_environment() {
  # Use the BATS_TEST_NAME env var to categorise all logging messages relating
  # to the clean-up activities
  export BATS_TEST_NAME="clean_test_environment"

  _run base_cleanup "${CE_TEST_IMAGE}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  fi
}

# Runs once before the first test
setup_file() {
  _run test_suite_setup clean_test_environment
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "test_suite_setup clean_test_environment"
    return 1
  fi

  # Call without run as we might export environment variables
  extra_setup
  status="${?}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "extra_setup"
    return 1
  fi
}

# Runs after the final test
teardown_file() {
  _run test_suite_teardown clean_test_environment
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "test_suite_teardown clean_test_environment"
    return 1
  fi
}

# bats file_tags=soafee
@test 'Check docker exist' {
  docker_binary=$(command -v docker 2>/dev/null || echo "")
  if [[ -z "${docker_binary}" ]]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

@test 'Check docker socket' {
  _run systemctl status -l docker.socket
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

@test "Check ${USER} user belong to docker group" {
  docker_group=$(id -nG | grep -ow docker || echo "")
  if [[ -z "${docker_group}" ]]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

# Run a container of the given image already put in cache.
# Execute an example workload simply consisting of an interactive
# shell, then remove it afterwards.
@test "Run a container with a persistent workload" {
  # Save it here so the rest of tests can use the IMAGE id
  get_image_id "${CE_TEST_IMAGE}" > "${TEST_IMAGE_ID}"

  local image_id
  image_id=$(cat "${TEST_IMAGE_ID}")

  _run image_run "-itd" "${image_id}" "/bin/sh"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi

  # Save it here so the rest of tests can use the container id
  echo "${output}" > "$TEST_CONTAINER_ID"
}

@test "Check that the container is running" {
  CONTAINER_ID=$(cat "$TEST_CONTAINER_ID")
  _run check_container_is_running "${CONTAINER_ID}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

# Run a non-detached container that ping to localhost
# It replaces previous test accessing the Internet. We don't need to test the
# Internet; just internal functionalities
@test 'Check ping to localhost in container' {
  CONTAINER_ID=$(cat "$TEST_CONTAINER_ID")

  engine_args="-i"
  cmd="ping -c 10 localhost"

  _run container_exec "${engine_args}" "${CONTAINER_ID}" "${cmd}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

@test "Remove the running container" {
  CONTAINER_ID=$(cat "$TEST_CONTAINER_ID")
  _run container_remove "${CONTAINER_ID}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

@test "Check the container is not running" {
  CONTAINER_ID=$(cat "$TEST_CONTAINER_ID")
  _run check_container_is_not_running "${CONTAINER_ID}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}
