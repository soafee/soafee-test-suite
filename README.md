# SOAFEE Test Suite

Tests used to verify software components interoperability and their adherence to the SOAFEE architecture standard.

## Status

The SOAFEE Test Suite is stable enough to prove at a high level the
functionality of any software stack. It has a set of minimal workload tests for
containers and virtualization that demonstrate how easily it is to add more
tests.

Please, refer to the [SOAFEE Test Suite documentation for more
details](https://soafee.gitlab.io/soafee-test-suite/).

## License

[MIT](https://choosealicense.com/licenses/mit/)

### SPDX Identifiers

Individual files contain the following tag instead of the full license text.

```text
  SPDX-License-Identifier: MIT
```

This enables machine processing of license information based on the SPDX
License Identifiers that are here available: http://spdx.org/licenses/
