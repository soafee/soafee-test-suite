PREFIX ?= /usr/local

LIB = lib
SOAFEE_DIR = share/soafee-test-suite
DEST_DIR := $(PREFIX)/$(SOAFEE_DIR)

LIB_FILES += $(LIB)/container-engine-funcs.sh
LIB_FILES += $(LIB)/integration-tests-funcs.sh
LIB_FILES += $(LIB)/login-console-funcs.expect
LIB_FILES += $(LIB)/run-command.expect
LIB_FILES += $(LIB)/soafee-test-suite-lib.sh
LIB_FILES += $(LIB)/virtualization-funcs.sh

PREFIX_LIB_FILES := $(addprefix $(DEST_DIR)/, $(LIB_FILES))

$(PREFIX_LIB_FILES): $(PREFIX)/$(SOAFEE_DIR)/% : %
	install -D $< $@

.PHONY: install
install: $(PREFIX_LIB_FILES)
	@# Don't keep ownership to avoid host contamination in Bitbake
	cp -R --no-preserve=ownership bin tests $(DEST_DIR)/
	@# Use relative symlink to avoid symlink-to-sysroot Bitbake warning
	mkdir $(PREFIX)/bin && \
	cd $(PREFIX)/bin && \
	ln -s ../$(SOAFEE_DIR)/bin/soafee-test-suite soafee-test-suite && \
	ln -s ../$(SOAFEE_DIR)/bin/soafee-test-suite-setup soafee-test-suite-setup && \
	ln -s ../$(SOAFEE_DIR)/bin/tap2lava tap2lava

.PHONY: uninstall
uninstall:
	@for f in $(PREFIX_LIB_FILES); do \
		[ -f "$$f" ] && rm -f "$$f"; \
	done
	[ -d $(PREFIX) ] && rm -rf $(PREFIX);

.PHONY: docs
docs:
	sphinx-build -b html docs html
